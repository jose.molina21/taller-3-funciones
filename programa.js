//función anonima para multiplicar
let multiplicar = function(a) {
    a = 15 * 8;
    console.log(a);
}

//función flecha para dividir
let dividir = (b) => {
    b = 1540 / 48;
    console.log(b);
}

//función callback para sumar dos numeros.
function sumar(d, e) {
    let f = d + e;
    console.log(f);
}

function call(devolver) {
    let g = 452;
    let h = 982;
    devolver(g, h);
}

call(sumar);